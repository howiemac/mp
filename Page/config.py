"""
config file for music app's custom Page

"""

show_who=False
show_when=False
show_time=False
show_rating=True
show_score=True
avatars=False
thumb_size=200


from grace.data.schema import *

class Page(Schema):
  table='pages'
  code=TAG,KEY
  parent=INT,KEY # defines a hierarchy
#  rel=INT,KEY    # allows an alternate hierarchy
  lineage=STR
  name=TAG,KEY
  kind=TAG,KEY
  stage=TAG,'live',KEY
  when=DATE,now,KEY
  text=TEXT,KEY
  seq=INT,KEY
  rating=INT,KEY
  prefs=TEXT
  score=INT,KEY
  backlinks=STR

# EXTRA FIELDS START HERE
  artist=TAG,KEY
  composer=TAG
  length=INT  # length in milliseconds
  version=TEXT,KEY

  insert=[
    dict(uid=1,parent=1,name='mp vlc',kind='root',lineage=".",stage="live"),
    dict(uid=2,parent=1,name='admin',kind='admin',lineage=".1.",stage="live"),
    dict(uid=3,parent=1,name='artists',kind='artists',lineage=".1.",stage="live"),
    dict(uid=4,parent=3,name='Various Artists',kind='artist',lineage=".1.3.",stage='posted'),
    dict(uid=5,parent=1,name='albums',kind='albums',lineage=".1.",stage='live'),
    dict(uid=6,parent=2,name='autolist',kind='playlist',lineage=".1.2.",stage='posted'),
    dict(uid=7,parent=1,name='albums',kind='albums',lineage=".1.",stage='live'),
    dict(uid=8,parent=1,name='playlists',kind='playlists',lineage=".1.",stage='live'),
    ]


