"""
music library override class for grace.page
"""

import random
import os
import sys
import urllib.request
import imghdr
import shutil
import vlc
from itertools import chain, zip_longest
from copy import copy as _copy

from grace.data import execute
from grace.render import html
from grace.serve.req import Req
from grace.lib import *
from grace.Page import Page as basePage


class Page(basePage):

  maxtracks = 500  # the maximum number of tracks (or items) allowed in listings
  maxauto = 200 # the number of tracks autoselected

  filekinds=basePage.filekinds+['track']
  imageaddkinds=basePage.imageaddkinds+['artists','artist','album','track']
  fileaddkinds=imageaddkinds
  playablekinds=['root','artist','album','track','playlist','smartlist']
  postkinds=basePage.postkinds+['artist','album','track','playlist','smartlist']
  ratedkinds=['artist','album','track','playlist','smartlist'] # kinds that a rating can be set for
  scoredkinds=['artist','album','track'] # kinds that a score is kept for
  playlistkinds=['root','playlist','smartlist']
  overviewkinds=['artist','album','track']
 
  validchildkinds={
   'artists':['artist'],
   'artist':['album'],
   'album':['track'],
   'track':[],
   'playlists':['smartlist','playlist'],
   }


  # the following override the grace.Page functions to  exclude disabled and deleted items
  # (O/S replace the grace versions with these? Maybe enhanced to allow the rating clauses to be optional?)

  def get_older_item(self):
        "for articles and replies - in date then uid order (seq is ignored) - get next visible sibling of same kind as self"
        if self.kind in self.postkinds:
            dt = self.when.sql()
            sib = self.list(
                parent=self.parent,
                kind=self.kind,
                stage='posted',
                where = f"(rating>=0) and (uid!={self.uid}) and ((`when`<{dt}) or (`when`={dt} and uid<{self.uid}))",
                limit=1,
                orderby='`when` desc, uid desc')
            return sib and sib[0] or None
        return None

  def get_newer_item(self):
        "for articles and replies - in date then uid order (seq is ignored) - get next visible sibling of same kind as self"
        if self.kind in self.postkinds:
            dt = self.when.sql()
            sib = self.list(
                parent=self.parent,
                kind=self.kind,
                stage='posted',
                where = f"(rating>=0) and (uid!={self.uid}) and ((`when`>{dt}) or (`when`={dt} and uid>{self.uid}))",
                limit=1,
                orderby='`when`, uid')
            return sib and sib[0] or None
        return None

  def get_next_alphabetical_item(self):
        "get next sibling, in name order, of same kind as self"
        if self.kind in self.postkinds:
            sib = self.list(
                parent=self.parent,
                kind=self.kind,
                stage='posted',
                where = f'(rating>=0) and (uid!={self.uid}) and (name>="{self.name}")',
                limit=1,
                orderby='name')
            return sib and sib[0] or None
        return None

  def get_previous_alphabetical_item(self):
        "for name order (seq is ignored) - get previous sibling of same kind as self"
        if self.kind in self.postkinds:
            sib = self.list(
                parent=self.parent,
                kind=self.kind,
                stage='posted',
                where = f'(rating>=0) and (uid!={self.uid}) and (name<="{self.name}")',
                limit=1,
                orderby='name desc')
            return sib and sib[0] or None
        return None

  def get_next_seq_item(self):
        "get next sibling, in seq order, of same kind as self"
        if self.kind in self.postkinds:
            where = f"(rating>=0) and ((seq>{self.seq}) or ((seq={self.seq}) and (uid>{self.uid})))"
            sib = self.list(
                parent=self.parent,
                kind=self.kind,
                stage='posted',
                where=where,
                limit=1,
                orderby='seq, uid')
            return sib and sib[0] or None
        return None

  def get_previous_seq_item(self):
        "get previous sibling, in seq order, of same kind as self"
        if self.kind in self.postkinds:
            where = f"(rating>=0) and ((seq<{self.seq}) or ((seq={self.seq}) and (uid<{self.uid})))"
            sib = self.list(
                parent=self.parent,
                kind=self.kind,
                stage='posted',
                where=where,
                limit=1,
                orderby='seq desc, uid desc')
            return sib and sib[0] or None
        return None


  # grace/Page/Page.py copy override:

  def copy(self,req):
      """ enhances basePage version to zero the score for any copied track
      - self is the destination: where the item is to be copied to
      - the item itself is stored in cache - accessed via classmethod get_move(req)
      - the difficulty is that we don't know the uids for any newly cloned tracks
      """
      #get the source and dest
      source=self.get_move(req)
      dest=self
      # do the standard grace copy
      res = basePage.copy(self,req)
      # identify any cloned track candidates
      tracks=[]
      if source.kind=="track": # we have cloned one track
        tracks=self.list(parent=dest.uid, kind="track", code=source.code)
      elif source.kind=="album": # we have cloned an album
        albums=self.list_int(item="uid",parent=dest.uid, kind="album", name=source.name, orderby="uid desc")
        if albums: # only use the newest
           tracks= self.list(parent=albums[0], kind="track")
      # recalculate the score from the plays table, rather than just zeroing it, just in case...
#      print(">>>>>> tracks: ",[t.uid for t in tracks])
      for track in tracks:
        track.recalc_score()
      return res

  # grace/Page/ratings.py overrides (now based on generic grace ratings code since Feb 2021)


## the following works but is not being used because it is now considered a bad idea - IHM 7 Apr 2022
#  def sync_ratings(self):
#      """ sync track ratings across all instances of the same audio file (unless disabled or deleted)
#          - expects req.rating (new rating)
#      """
#      if self.kind=="track":
#        tracks=self.list(code=self.code, kind="track")
#        for track in tracks:
#          if (track.uid==self.uid) or (track.rating>0):
#            track.rating=self.rating
#            track.flush()
#      else: # we already have the new self.rating, so...
#        self.flush()
#
#  def rate_up(self,req):
#      "increase rating for self and (if track) other instances of the same audio file (code)"
#      self.rating=self.upratings[self.rating_index()]
#      self.sync_ratings()
#      return self.rating_return(req)
#
#  def rate_down(self,req):
#      "decrease rating for self and (if track) other instances of the same audio file (code)"
#      self.rating=self.downratings[self.rating_index()]
#      self.sync_ratings()
#      return self.rating_return(req)

  def toggle_disable(self,req):
    """ override Page.ratings.py version to:
        - disable/enable all albums when disabling/enabling an artist
        - disable/enable all tracks when disabling/enabling an album
        - thus disabling an artist will disable all of its albums and all of its tracks
        - deleted items (rating is -5) are unaffected
        - BEWARE: when disabling, child items (albums and tracks) already disabled will lose that distinction, and will be re-enabled when the parent is re-enabled
        - SO: Don't use disabling of individual tracks or albums as a long term means of suppressing them: Instead rate them as X and they won't affected by disable/enable
    """
    def disable_tracks(album):
        "disable all tracks for self"
        for track in self.list(parent=album.uid,kind="track"):
          if track.rating in (0,1,2,3):
            track.rating-=4
          track.flush()

    def enable_tracks(album):
        "enable all tracks for self"
        for track in self.list(parent=album.uid,kind="track"):
          if track.rating in (-4,-3,-2,-1):
            track.rating+=4
          track.flush()

    def disable_albums(artist):
        "disable all albums and their tracks for self"
        for album in self.list(parent=artist.uid,kind="album"):
          if album.rating in (0,1,2,3):
            album.rating-=4
          album.flush()
          disable_tracks(album)

    def enable_albums(artist):
        "enable all albums and their tracks for self"
        for album in self.list(parent=artist.uid,kind="album"):
          if album.rating in (-4,-3,-2,-1):
            album.rating+=4
          album.flush()
          enable_tracks(album)

    if self.kind=="artist":
        # disable or re-enable all albums and their tracks for this artist
        if self.rating in (0,1,2,3):
          disable_albums(self)
        elif self.rating in (-4,-3,-2,-1):
          enable_albums(self)
    elif self.kind=="album":
        # disable or re-enable all tracks for this album
        if self.rating in (0,1,2,3):
          disable_tracks(self)
        elif self.rating in (-4,-3,-2,-1):
          enable_tracks(self)
    # disable or enable self and return
    return basePage.toggle_disable(self,req)

  # the following could / should be a generic function in Page.ratings.py ??

  def get_rating_filtered_children_by_kind(self, kind="", orderby='seq,uid'):
      "get all children of given (or own) kind where rating included in rating_filter set"
      return self.list(
          parent=self.uid, kind=kind or self.kind, where=f"rating in {sql_list(self.rating_filter())}", orderby=orderby, _debug=False)

  # overridden as grace version is obsolete (and should be replaced with this)

  def edit_image(self, req):
        "(simpler) image edit handler"
        req.page = 'edit'
        if "name" in req:  #we have an update
            # replace filedata
            if req.filedata:
                if not self.image_saved(
                        req,
                        replace=True):  # save new image under existing uid
                    return self.edit_tag(req)  # give error
                req.width = ''  # we don't want the old width
            # store changes
            self.update_image(req)
#            return self.get_pob().redirect(req, '')
        return self.get_pob().redirect(req, 'add_image')

  # extra data #################
  
  def save_text(self,req):
    "deal with extra items - notably the track dates"
    #self.update_tags(req)
    # ensure that track/album has an artist  
    if not req.artist:
      if self.kind=='track': 
        req.artist=self.get_pob().get_pob().name  # use the album artist
      elif self.kind=='album':
        req.artist=self.get_pob().name
    o=basePage.save_text(self,req)# this does, inter alia, an update(req) and flush()
    # now that we have updated self.when, update the children, if requested
    if req.trackdates and (self.kind=='album'): # make the track dates (when) the same as the album date
      for i in self.get_children_by_kind(kind='track'):
        i.when=self.when
        i.flush()
    return o


  # handling of extra kinds #############

  def set_seq(self):
    "override for set_seq"
    if self.kind=='track':
      pass # tracks use seq for track number, so don't allow change here
    else:
      basePage.set_seq(self)

  def get_order(self, order_by=''):
    "override for the basePage version - to include artist and score orders"
    p=order_by or self.get_pref('order_by')
    if p=='artist':
      order='artist,name'
    elif p=='score':
      order='score desc'
    elif p=='latest':
      order='uid desc'
    else:
      order=basePage.get_order(self,p)
    return order

  def add_page(self,req):
    "override the default to add new genres"
    # add required data to req
    if req.kind=='album':
      req.artist=self.name
    elif req.kind=='track':
      req.artist=self.artist
    # do the default create
    page=self.create_page(req)
    # fix for adding genres, and return
    if page and req.name and self.name=='genres':
      page.add_tag(req.name) # add the new genre, by adding a tag for it
      req.message="genre %s added" % req.name
      return self.view(req)
    return page.redirect(req,'edit') # default is to return new page in edit mode

  def can_move_here(self,req):
    "is it okay to move or copy the  move object here - can be overridden by inheriting classes"
    move=basePage.can_move_here(self,req)
    if move and (move.kind!="image") and self.kind in self.validchildkinds and not move.kind in self.validchildkinds[self.kind]:
      move=None
    return move

  def get_smart_playlist(self,req=None,objects=False,info=False):
    '''return a list of track uids (or objects, if requested) per smartlist criteria

    if info is True, will return only the search criteria

    can set artist and/or tabs and/or (in self.text) where/orderby/limit sql

    self.text can (legitimately) contain:
      - where clause
      - order by
      - limit
    '''
    # get tags, if any
    tags=self.get_tags()
    select="select pages.%s from pages " % (objects and '*' or 'uid',)
    if tags:
      select+="inner join tags on tags.page=pages.uid "
      if len(tags)==1:
        tagclause="tags.name='%s'" % tags[0]
      else:
        tagclause="tags.name in %s" % str(tuple(tags))  
    else:
      tagclause=""
    # add artist, if any
    artistclause= self.artist and ("artist='%s'" % self.artist) or ""
    # add rating clause - show items with a rating in rating_filter
    rat=self.rating_filter()
    ratingclause= f"rating in {sql_list(rat)}"
    # add text, if any - allow a starting "where " but ignore it
    t=self.text.strip()
#    print("text:",t)
    if (t.startswith('order by') or t.startswith('limit')):
      textclause=""
      finalclause=t
    else:
      textclause=t.startswith('where ') and t[6:] or t
      finalclause=""
    # are we just returning info?
    if info: # we are done - return the criteria
      wheres=' and '.join(i for i in (tagclause,artistclause,ratingclause,textclause) if i)
      return "tracks %s%s %s" % ('where ' if wheres else "",wheres,finalclause)
    # put it all together and execute the query
    where="where %s " % (' and '.join(i for i in ("kind='track'",tagclause,artistclause,ratingclause,textclause) if i),)
    sql=select+where+finalclause
#    print("SQL is:",sql)
    try:
      if objects:
        sl=[self.get(i['uid'],data=i) for i in execute(sql)]
      else:
        sl=[i["uid"] for i in execute(sql)]
    except BaseException as e:
      sl=[]
      if req:
        req.smartlist_error='invalid query: %s' % e
      else:
        print("<<<< EXCEPTION >>>>", e)
#      raise
    return sl

  def get_dumb_playlist(self,objects=False):
    '''gets list of track uids (or objects, if requested) from playlist or root kind (as opposed to a smartlist, or album playlist)

       also checks for invalid items, and removes them from the playlist (brute force method - does this every time!)
    '''
    rawuids=[safeint(i) for i in self.text.split('\n')]
    uids=[uid for uid in rawuids if uid]
#    print ">>>> uids=",uids
    playlist=[]
    ok=True
    for i in uids:
        try: # make sure it exists
            x=self.get(i)
        except:
            x=False
        if x and x.kind=='track': # add it to the playlist
            playlist.append(x)
        else: #flag to update the playlist
            ok=False
#    print ">>>> dumb playlist=",playlist
    if not ok: #fix the playlist
        self.text='\n'.join([str(i.uid) for i in playlist])
        self.flush()
#    print ">>>> fixed dumb playlist=",[i.uid for i in playlist]
    # filter for rating
    if self.kind!='root': # no rating filter for the root playorder
      playlist=[i for i in playlist if i.rating in self.rating_filter()]
#      if not objects: # filter the uids only if we need to
#        uids=[i.uid for i in playlist]
#      print ">>>> filtered dumb playlist=",[i.uid for i in playlist]
    return playlist if objects else [i.uid for i in playlist]

#  def get_playorder(self):
#    """ returns simply the uids of a dumb playlist 
#     No checking or filtering.
#     Used for the play order: uid=1, kind=="root"
#    """
#    uids=[safeint(i) for i in self.text.split('\n') if safeint(i)]
#    return uids


  def get_album_playlist(self):
    "returns list of all track objects with rating in rating_filter, on that (self) album, in seq order"
    return self.list(parent=self.uid,kind='track',where=f'rating in {sql_list(self.rating_filter())}',orderby='seq,uid')

  def get_artist_playlist(self):
    "returns list of all track objects with rating in rating filter, by that artist (self), in current pref album sort order, and album seq within that"
    playlist=[]
    for album in self.list(parent=self.uid,kind='album',orderby=self.get_order()):
      playlist.extend(album.get_album_playlist())
    return playlist

  def get_playlist_section(self,req):
    """return a list of 50 playlist objects from req._pl_start
     optional param: req._pl_start
    """
#    if self.kind=='root':
#      playlist=self.get_playorder()
    if self.kind in ('root','playlist'):
      playlist= self.get_dumb_playlist(objects=True)
    else:# must be a smart playlist
      playlist=self.get_smart_playlist(req,objects=True)
    if playlist:  # if we have at least one integer value
      # set start and end
      start=safeint(req._pl_start)
      # find current index
      curr=self.transport.uid
      p=0 if curr else -1
      # if we are playing a track and we are in the root or current playlist...
      if curr and ((self.uid==1) or (self.player.list and (self.uid==self.player.list.uid))):
        # adjust _pl_start to display the currently playing track near the top of the list if possible
        # try using the index to find the track in the playlist - this should work
        try:
          p=self.get_index()
          assert playlist[p].uid==curr
        except:
          p=0
#          raise
          # otherwise, look for the currently playing track by uid
#          try:
#            p=playlist.index(self.transport.uid) # find the current track
#          except:
#            raise
#            p=0
#        print "got p:",p
        if ("_pl_start" not in req) and (p>start):
          start=max(0,p-2) # adjust start so that the current track is third or less on the list
      # adjust end, and set more req parameters
      end=start+50
      req._pl_index=p
      req._pl_start=start
      if start:
        req._pl_back=max(0,start-50)
      if len(playlist)>end:
        req._pl_more=end
      req._pl_len=len(playlist)
#      print "start=",start,"  end=",end," index=",req._pl_index,"  more=",req._pl_more,"  back=",req._pl_back, " len=",len(playlist)
      return playlist[start:end]
    return []

  def get_playlist(self):
      "get a playlist for playing (or playnext)"
      # call the appropriate playlist constructor
      if self.kind=='track':
        playlist=[self]
      elif self.kind=='album':
        playlist=self.get_album_playlist()
      elif self.kind=='artist':
        playlist=self.get_artist_playlist()
      elif self.kind=='smartlist':
        playlist=self.get_smart_playlist(objects=True)
      elif self.kind in ('root','playlist'):
        playlist=self.get_dumb_playlist(objects=True)
      # shuffle, if necessary
#      if len(playlist)>1 and self.player.is_shuffling:
      if len(playlist)>1 and hasattr(self,"shuffling"):
        random.shuffle(playlist)
      # and return
      return playlist

  def get_images(self):
    "enhance grace.Page.py - so that track with no images inherits from album, and album from artist"
    images=basePage.get_images(self)
    if not images and self.kind in ('track','album'):
      pob=self.get_pob()
      image=pob.get_image()
      if image:
        image.composer=pob.url() #store linkto album in here
        images=[image]  # don't cache the inherited image in self.images - so self.images can be used to get only the child images
    return images



  # auto (i.e. smart) playlist ##############################################

  def get_autolist(self,req):
      "builds a new auto playlist of self.maxauto (or less)"
      if self.uid == self.player.autolist: # safety first..
          ratings = self.rating_filter()
#ex          freshmaxscore= max(ratings * 10,2)
          fresh = self.list_int(
            kind="track",
#            where=f"rating in {sql_list(ratings)} and score<(rating*10+2)",
            where=f"rating in {sql_list(ratings)} and score<((1+rating)*(1+rating)*2)",
            orderby="random()",
            limit=self.maxauto // 2
            )
          choice = self.list_int(
            kind="track",
            where=f"rating in {sql_list(ratings)}",
            orderby="random()",
            limit=self.maxauto - len(fresh)
            )
          # interleave the lists - using itertools chain() and zip_longest()
          lists = [fresh, choice]
          alist = [uid for uid in chain(*zip_longest(*lists)) if uid is not None]
          # store the uids as text
          self.text = '\n'.join(map(str,alist))+'\n'
          self.flush()


  # score (plays) #################################
  

  def add_play(self,when,times):
    "adds play(s) to the score for self (a track), album, and artist"
    # add the play to plays table
    ob=self.Play.new()
    ob.when=when
    ob.times=times
    ob.page=self.uid
    ob.flush()
    # add to track score
    self.score=self.score+times
    self.flush()
    # add to album score
    album=self.get_pob()
    album.score=album.score+times
    album.flush()
    # add to artist score
    artist=album.get_pob()
    artist.score=artist.score+times
    artist.flush()

  @html
  def play_history(self,req):
    "show plays and play log history for this track"
    pass

  @html
  def recent(self,req):
    "list recent track plays (all tracks, from play log, latest first)"
    # fetch the plays
    req.data=[]
    for play in self.Play.list(orderby='`when` desc',limit=page(req,self.maxtracks)):
      try:
        ob=self.get(play.page)
        ob.played=play.when 
        req.data.append(ob)
      except:
        pass  # track has presumably been deleted  

  def fetch_plays(self,logfiles=[]):
    "get SCORES (plays) AND RATINGS from log file, and store in Play instances and in Pages - called from add_logs() below"
    #format line sample: 7 played at 11/06/2013 15:06:30 *22
    ln=0
    fails=0
    playuid=0
    for logfile in logfiles:
      f=open(logfile,'r')
      log=f.read().split('\n')
      print("LOG ITEMS=",len(log))
      validated=False
      for i in log:
        vals=i.split()
        if len(vals)>4: # otherwise we have an empty or invalid line, so skip it
          if len(vals)==5: # original format
            uid,act,dummy,date,time = i.split()
            val=1
          else: # current and mphistory format
            uid,act,dummy,date,time,val = vals
            if val[0]=='*': # mphistory version
              val=safeint(val[1:])
            else: # current version
              val=safeint(val)  
          # check validity
          page=safeint(uid)
          when=DATE(date+" "+time)
          if (act!='rated') and (not validated): # crude but reasonably effective... 
            if self.Play.list(page=page,when=when,times=val): 
              print("duplicate log: %s" % (logfiles[ln],))
              fails+=1  
              break
            validated=True  
          # log it in plays (if it is a play)
          if act.startswith('play'):
           if self.list(uid=page): # make sure it still exists...
            self.get(page).add_play(when,val)
#            ob=self.Play.new()
#            if not playuid:
#              playuid=ob.uid # first new uid - passed to set_scores() to make it only use the newly added play logs          
#            ob.when=when
#            ob.page=page
#            ob.times=val
#            ob.flush()
#            print 'line ',ln,' uid=',ob.uid,'score=',ob.score
          elif act=='rated': # update Pages ratings crudely - i.e. REGARDLESS OF ANY POSSIBLE MORE RECENT CHANGE HAVING BEEN MADE...
            try:
              pob=self.get(page)
              pob.rating=val
              pob.flush()
            except:
              print("could not find page %s" % page)
              pass # we don't care if the page no longer exists....
      ln+=1
 #   if  playuid:
 #     self.set_scores(playuid)  # updates Pages based on Plays
    print("=== %s logs fetched successfully, %s log duplicates ignored ===" % (ln,fails))
    msg="%s logs processed, %s duplicates ignored" % (ln,fails)
    return msg

 
  # preferences ###################################
 
  # adopt the standard page preferences, and extend them
  # {kind:{name:(default,display-name,display-type/size/options),},}
  # Note: python copy.copy has been imported as _copy so as not to clash with our own def copy() override above...
  basePage.default_prefs.update((
   ('artists',_copy(basePage.page_default_prefs)),
   ('albums',_copy(basePage.page_default_prefs)),
   ('playlists',_copy(basePage.page_default_prefs)),
#   ('playlist',_copy(basePage.page_default_prefs)),
   ('artist',_copy(basePage.page_default_prefs)),
   ('album',_copy(basePage.page_default_prefs)),
   ('track',_copy(basePage.page_default_prefs))
   ))
  basePage.default_prefs['track'].update({
  'starttime' : (0,'start time (seconds)',4),
  'stoptime' : (0,'stop time (seconds)',4),
  'volume' : (100,'volume %',3)
  })
  basePage.default_prefs['album'].update({
  'volume' : (100,'volume %',3)
  })
  basePage.default_prefs['album']["order_by"]=('seq','order items by',('date','latest','name','seq')) # override the default
  basePage.default_prefs['artist']["order_by"]=('score','order items by',('score','date','latest','name','seq')) # score added as default
  basePage.default_prefs['artists']["order_by"]=('score','order items by',('score','date','latest','name','seq')) # score added as default
  basePage.default_prefs['albums']["order_by"]=('score','album display order',('score','artist','name','date','latest','uid')) # score added as default
  #basePage.default_prefs['smartlist']=({
  #'order_by':('','order by',('','artist','name','date','latest','score','rating')),
  #'min_rating':('0','minimum rating',('2','1','0','-4')),
  #'max_rating':('2','maximum rating',('2','1','0','-4')),
  #})

  # 22/12/2021: The following preference routines were written as an upgrade for grace itself, but the ancestor inheritance is not
  #  desirable for the default preferences, and so the grace version has been stripped down.

  def get_prefs(self):
        """
        returns dictionary of preferences for self, from cache if possible
        - self.get_pref() should be used for userspace access to preferences

        returns:
        - returns all valid keys
        - returns local values, where present, else None values

        does not return:
        - ancestor and config and default values: these  must be obtained subsequently via get_pref()

        approach:
        - converts the text definition in self.prefs (if any) to a cached dictionary: self_prefs
        - any missing keys are added from the default_prefs, and given None values
        - ancestor and config and default values are subsequently obtained by get_pref()
        """
        if not hasattr(self, '_prefs'):
            self._prefs = {}
            if self.kind in self.default_prefs:
                # get default prefs for this page kind
                defs = self.default_prefs[self.kind]
                # convert the self.prefs text to a self._prefs dictionary
#                print(self.uid,"prefs text:", self.prefs)
                if self.prefs:
                    for i in self.prefs.split('\n'):
                        if i:
                            k, v = i.split('=')
                            if k in defs: # we ignore any old keys now removed from default_prefs
                                if not v and (
                                        defs[k][2] != 'checkbox'
                                ):  # non-checkboxes require a value
                                    v = None
                                self._prefs[k] = v
                # add any missing keys from the defaults
                for k in defs.keys():
                    if k not in self._prefs:
                        self._prefs[k] = None
        return self._prefs

  def get_pref(self, pref, derived=True):
        """
        returns the value for the relevant pref (preference key)

        if derived == False, the local value is returned OR "".

        If derived == True, the value is derived as follows:
        - self.prefs (local value if any)
        - OR ancestor (container) prefs, via lineage reversed
        - OR self.default_prefs[self.kind]
        - OR ""
        """
#        print(f"getting {derived and 'derived' or 'local'} pref: ",pref, " for " ,self.kind,self.uid)
        p = None
        if self.kind in self.default_prefs:  # check own prefs
            p = self.get_prefs().get(pref) or None
        else: # invalid kind
            return ""
#        print("checking self: ",repr(p))
        if not derived:
#            print(f"RETURNING (for {self.uid}):",pref,p)
            return p or ""
        if p is None:  # check up along the lineage
            lineage = reversed(self.lineage.strip(".").split("."))
#            print(">>> lineage = ",list(lineage))
            for l in lineage:
                if l:
                    container = self.get(safeint(l))
                    if container.kind in self.default_prefs:  # check container's prefs
                        p = container.get_prefs().get(pref) or None
#                        print("checking lineage: ",container.uid, container.name,"=>", repr(p))
                        if not p is None:
                            break
        if p is None: # use default
            defs = self.default_prefs[self.kind]
            p = defs[pref][0]
#            print("using default: ",pref, p)
        return p or ""

  def update_prefs(self, req):
        "called by Page_preferences.evo: updates self.prefs"
        xprefs = self.get_prefs()
        self.prefs = ''
        for name, defn in list(self.default_prefs[self.kind].items()):
            default, displayname, typ = defn
            value = req.get(name, '').strip()
            #      print "======",name,':',value,' ( ',req.get(name,''),' )'
            self.prefs += '%s=%s\n' % (name, value)
            # make any changes necessary
            if (xprefs.get(name) != value) and hasattr(self,f"change_{name}"):
                getattr(self, f"change_{name}")(req)
        self.flush()
        del self._prefs  # clear cache
        # HOOK UNIQUE TO THIS music APP
        if self.kind=="track":
            # in case the starttime or stoptime preferences have changed,
            #  ensure that any such change is reflected in self.length
            self.calc_length()
        # end of HOOK
        return req.redirect(self.url())

  update_prefs.permit = 'create page'

  # THE FOLLOWING are CURRENTLY UNIQUE TO THIS music APP

  def set_pref(self, pref, value):
        "updates a single pref in self.prefs - DOES NOT FLUSH"
        prefs = self.get_prefs()
        prefs[pref] = value
        self.prefs = ''
        for name, value in list(prefs.items()):
            self.prefs += '%s=%s\n' % (name, value)

  def get_timepref(self,name):
     """ return timepref as an integer in seconds
         name must be "starttime" or "stoptime"
         if in minutes:seconds format, converts this to seconds

         All uses of " starttime" and "stoptime" must now be accessed via this method.

         Thus the convenient minutes:seconds format can be used when setting these preferences.
     """
     time=self.get_pref(name)
     if ":" in time:
       m,s= time.split(":")
       time=safeint(m)*60+safeint(s)
     else:
       time=safeint(time)
     return time


#  # theme  #################
#
#  def theme_url(self,):
#    "override theme_url - CACHED"
#    if not hasattr(self,"_theme_url"):
#      self._theme_url="/site/theme_%s" % self.get(2).get_pref("theme")
#    return self._theme_url
#
#
#  def change_theme(self,req):
#    ""
#    os.unlink("../htdocs/site/theme")
#    os.symlink("theme_%s" % req.theme,'../htdocs/site/theme')    



# media player ######################################
#

  # set up media player
  player=vlc.MediaListPlayer()

#  instance=player.get_instance()
#  logfile=open('VLCerrors.log','w')
#  instance.log_set_file(logfile)

  player.autolist=6

  transport=vlc.MediaPlayer()
#  transport.audio_output_set("pulse") # doesn't work... returns 0
  transport.uid=False  # this will store the uid of the currently playing track
#  transport.prevuid=False    # this will store the uid of the previous track played (if any) NOT CURRENTLY USED
  transport.nextuid=False    # this will store the uid of the next track to be played (if any)
  transport.tracks=0 # this will store the size of the current playlist
  transport.newtrack=False # this will indicate a track change
  transport.log_play=True # flag to allow logging of plays
  transport.stoptime=0 # this will store the stoptime (if any) of the curretly playing track
  player.set_media_player(transport) # associate the transport with the player
  player.set_playback_mode(0) # enforce the default (rather than loop or repeat)


#  transport.audio_set_volume(100) # ie 100%
#  print ("volume is:", transport.audio_get_volume())


# note  I attempted to set volume here...
#
#  transport.audio_set_volume(50) # ie 40%
#  print ("volume is:", transport.audio_get_volume())
#  
# note  transport.audio_set_volume(v) DOESN'T WORK!!! - the volume remains at 100% regardless of the value of v
#       Interestingly, transport.audio_get_volume() will return v correctly (i.e. what was set)
#       Also, where v is 0 it DOES work! Otherwise it always gives 100% volume, regardless of v...
#
# UPDATE - actually it does work - it is setting the app volume as per pulseaudio, but my pulseaudio setup is bodged
#       in way that gives me top quality sound through my USB Transit by (somehow) byassing the pulseaudio app volumes!
# 	So, pulseaudio is the culprit!!!
#
#       Is "volume" a discontinued feature?
#       cvlc at the command line, says "--volume no longer exists" !
#       They now use "--gain" eg "--gain=0.2" which works in cvlc, but not as an option for instance.media_new()...
#       There are also various cvlc / vlc options for "audio_replay_gain_???", i.e. uniform volume per track / album...
#       There are no functions for "gain" in vlc.py . Is it just out of date?

  player.list=None  # once set, will be the current list of vlc media items - eg: player.list[0].get_mrl() will give url of track 0 in the list
  player.mode= 0 # default - not looping or repeating

  player.stop() # this seems to fix VLC no-sound glitch on startup...

  # set up our cached states, which will be maintained by get_player_state() and pause()
  player._state="stopped"
#  player.is_shuffling=False # DEPRECATED
  player.is_paused=False
  player.timeleft=False # for tracktime display: False will give time elapsed, True gives time remaining
  # get instance, for general use
  instance=player.get_instance()


# track change handling ############################################

  @vlc.callbackmethod
  def event_next_item(event,transport):
    "set transport.newtrack flag upon a next_item event"
#    print(event.__dir__())
#    print("EVENT: ",event.type)
#    print("OBJECT: ",event.object)
#    print("............................... next item")
    transport.newtrack=True
  # define the callback
  player.event_manager().event_attach(vlc.EventType.MediaListPlayerNextItemSet,event_next_item,transport)

# possible other events to use?
##  player.event_manager().event_attach(vlc.EventType.MediaListPlayerPlayed,event_played)
##  player.event_manager().event_attach(vlc.EventType.MediaListPlayerStopped,event_stopped)


  def newtrack(self):
    "does common handling for a track change"
    playing=self.player.is_playing()
    if self.transport.uid and (self.transport.newtrack or not playing):
      # the track has changed, so log the previous track
      self.log_track(self.transport.uid)
#      print('state = ',self.get_player_state())
      if playing:
#        print('storing newtrack data')
        # update self.transport properties for the new track - this also resets our "newtrack" flag
        self.store_newtrack_data()
      elif not self.player.is_paused: # presumably we are at the end of the playlist, so force a stop
#        print("stopping......")
        self._stop()
      return True
    # print "======= polling for track-change \n",
    return False

  def store_newtrack_data(self):
    "updates self.transport properties after a track change"
    # recognise the track change by clearing the "newtrack" flag
    self.transport.newtrack=False
    # update the current uid
    uid=self.uid_now_playing()
    self.set_transport_uids(uid)
    # calculate and store stoptime preference for efficient reuse
    self.transport.stoptime=self.get(uid).get_timepref("stoptime")*1000
    return True

  def set_transport_uids(self,uid):
    "maintain info on current, and next track: cached in self.transport"
    t=self.transport
#    t.prevuid=self.prev_played()
    # set current uid regardless (as may be False, which is valid)
    t.uid=uid
    # set next uid
    if t.uid:
      t.nextuid=self.next_up()
    return True

  # following is currently used only to give trackname title for transport skip forward button
  def next_up(self):
    "uid of next track on playlist (if any)"
    nup=False
  #  print "self.transport.uid= ",self.transport.uid
  #  print "self.player.is_paused= ",self.player.is_paused
    uid=self.transport.uid
    if uid:
        playlist=self.get(1).text.split('\n')
        try:
            p=playlist.index(str(uid))
        except:
            p=-1
        if (p>=0) and ((p+1)<len(playlist)):
            nup=safeint(playlist[p+1])
    return nup

# not used
#
#  def prev_played(self):
#    "uid of last-played track (from play log)"
#    try:
#      logged=self.Play.list(orderby='`when` desc',limit="1")[0]
#      pp=logged.page
##      print ">>>>>",pp
#    except:
##      raise
#      pp=False 
#    return pp


  def prepare_media(self):
    """prepare a media obj for adding to medialist - handle start and stop times
    """
    start=self.get_timepref('starttime')
    stop=self.get_timepref('stoptime')
#    volume=limit(self.get_pref('volume'),0,200) # use limit() to ensure that volume pref is sensible...
    m=self.instance.media_new(self.mrl())
# NONE of the following options work
#    m=self.instance.media_new(self.mrl(),'gain=0.2')
##    m=self.instance.media_new(self.mrl(),'sout-raop-volume=%s' % volume)
#    m=self.instance.media_new(self.mrl(),'audio-replay-gain-mode=track','--audio-replay-gain-default=0.2')
    if start:
        m.add_options('start-time=%s' % start) 
    if stop:
        m.add_options('stop-time=%s' % stop) 
# the following test code DOES NOT WORK, though it does in cvlc at the command line, eg > cvlc my.mp3 --gain=0.2
#    gain="1.5"
#    print "SETTING GAIN for %s at %s%%" % (self.uid,gain)
#    m.add_option('gain=%s' % gain)
    return m

  def get_state(self):
    "get VLC state data about currently playing item - DO WE NEED THIS FOR ANYTHING?"
    states={
     vlc.State.Buffering:"buffering",
     vlc.State.Ended:'ended', 
     vlc.State.Error:'ERROR',
     vlc.State.NothingSpecial:"ok",
     vlc.State.Opening:"opening",
     vlc.State.Paused:"paused",
     vlc.State.Playing:"playing",
     vlc.State.Stopped:"stopped",
     }
    if self.player.list:
      state=states.get(self.transport.get_media().get_state(),"other")
    else:
      state="off"
    return state


  def get_player_state(self):
    "get our required state info: 'stopped', 'playing', 'paused', or 'ended'"
    # here is how to identify the 4 listplayer states that we care about:
    # - stopped:  (not self.player.list)
    # - playing:  self.player.list and self.player.is_playing()
    # - paused:   self.player.list and self.player.is_paused
    # - ended:    self.player.list and not (self.player.is_playing() or  self.player.is_paused)
    #note: player.is_paused is set by def pause()
    #note: we don't want the 'ended' state, so we will 'stop' ASAP - see newtrack() 
    if self.player.list:
        if self.player.is_playing():
          s='playing'
        elif self.player.is_paused:
          s='paused'
        else:
          s='ended'  
    else:
        s='stopped'
    return s


#  def uid_now_playing(self):
#    """give uid of currently playing track from the transport
#       NOTE - this is based on the code, so is wrong for copied tracks
#    """
###     - brute force method required in absence of functioning callbacks.... WORKS but not required now we know about transport
##    current=0
##    for i in self.player.list:
##      if i.get_state() in (vlc.State.Buffering,vlc.State.Opening,vlc.State.Paused,vlc.State.Playing,vlc.State.Stopped):
##        current=i.get_mrl()
##        current=current.rsplit("/",1)[1][:-4]
##        break
##    return safeint(current)
#    self.transport.media=self.transport.get_media()
#    if self.transport.media:
#        mrl=self.transport.media.get_mrl()
##        print ">>>>>>>>>>>>>>>",mrl 
#        return safeint(mrl.rsplit("/",1)[1].rsplit(".",1)[0]) # extract the uid as text, and convert to int
#    return 0

  def uid_now_playing(self):
    """give uid of currently playing track from the transport
       allows for copied tracks
       rough but works for simple cases...
    """
    self.transport.media=self.transport.get_media()
    if self.transport.media:
        mrl=self.transport.media.get_mrl()
 #       print("mrl: ",mrl)
        code=mrl.rsplit("/",1)[1]
 #       print("code: ",code)
        uids= [track.uid for track in self.list(code=code,kind="track")]
 #       print("uids: ",uids)
        playlist=[safeint(i) for i in self.get(1).text.split('\n')]
 #       print("playlist: ",playlist) 
        for uid in uids:
          if uid in playlist:
             return uid
    return 0



  # AJAX handlers ##########################################################

  def toggle_time_display(self,req):
    "toggle between time remaining (left) and time elapsed"
    self.player.timeleft = not self.player.timeleft
    return ""  # result of this function is not used

  def tracktime(self,req):
    "AJAX handler - returns formatted string showing current track-time (elapsed or remaining)"
    def formatted(ms):
        "formats milliseconds in minutes and seconds"
        if ms<0:
          return "-:--"
        s=ms//1000
        m,s=divmod(s,60)
        return "%d:%02d" % (m,s)
    left=self.time_left()
    ms= left if self.player.timeleft else self.transport.get_time()
    t=formatted(ms)
#    print(">>>> tracktime poll:",t, f"({ms})"," left=",left,)
    return t

  def trackchange(self,req):
    "AJAX handler - returns 'yes' when there has been a track change, or playlist is ended, requiring a page refresh"
    if self.newtrack():
      return "yes"
#    print("====== poll trackchange again in %s ms" % max(self.time_left(),2000))
    return str(max(self.time_left(),2000))  # poll again in 2 seconds or later (when we think the track will be ended)

  def bgtrackchange(self,req):
    "AJAX handler - logs background track change - returns milliseconds until next check"
    self.newtrack() # we don't care about the result
#    print(f"============ poll bgtrackchange again in {self.time_left()} ms")
    return str(self.time_left())




  # views and overviews #################################################

  def auto(self,req):
    """auto-show the currently-playing track
    - redirects recursively to the currently playing track, then returns view() for that track
    - forces a stop when playlist is finished, and returns to playlist page
    - if already stopped, will start autoplay when self is the default autoplay playlist
    - sets req.auto to the currently playing track
    """
    if self.player.is_playing():
      # check for track change, and process it before changing the transport.uid
      if not self.newtrack():
        # we have no track change, so make sure our transport.uid is valid
        self.set_transport_uids(self.uid_now_playing())
      # store the currently-playing track object as req.auto
      req.auto=self.get(self.transport.uid)
      # if self is the current track, then return self.view()
      if (req.auto.uid==self.uid):
        req.return_to=self.url("auto")
        return self.view(req) # update the track display
      # otherwise recurse to show the currently playing track
      return req.auto.redirect(req,"auto")
    elif self.player.is_paused:
      req.auto=self.get(self.transport.uid)
      return self.view(req) # update the track display
#      return self.get(self.player.is_paused).redirect(req) # show the currently paused track
#   if we get here, then presumably the player has stopped...
    # start playing autolists , if requested..
    if self.uid==self.player.autolist:
      self.get_autolist(req)
      return self.redirect(req,"play")
    # otherwise, show "from" playlist 
    frm=self.player.list.uid if self.player.list else 1
    return self.get(frm).redirect(req,"")


  def update_for_viewing(self,req):
    "update some data before viewing or overviewing"
    # flag whether page should be refreshed when track changes 
    # - ie whether self is the current track or album or artist
    cuid=self.transport.uid
    uids=(cuid,1,self.player.list.uid) if self.player.list else (cuid,)
    req.refresh=self.uid in uids
#    print "req.refresh:",req.refresh
    # get req.data if necessary
    if self.kind in self.playlistkinds:
      req.data=self.get_playlist_section(req,)
    # check album length, if an album
    # - Brute force approach for certainty...
    # - This covers track additions, deletions, and moves...
    if self.kind=='album':
      self.check_album_length()
    elif self.kind=="playlist":
      self.calc_length()
    # similar for score (plays) for artists and albums
    if self.kind in ('album','artist'):
      self.update_score()

  def set_req_vars(self,req): 
    # set the req variables (as per grace page.py def view_tag)
    req.pages = self.get_child_pages(req)  # for Page_view_tag.evo
    req.pageuid = self.uid  # for use in templates to test whether a given instance is the page instance
    req.page = "view"
    req.tab = self.kind
 
  @html
  def track_overview(self,req):
    ""
    self.set_req_vars(req)
#    req.wrapper="wrapper_subtle.evo"
 
  @html
  def album_overview(self,req):
    ""
    self.set_req_vars(req)
#    req.wrapper="wrapper_subtle.evo"

  @html
  def artist_overview(self,req):
    ""
    self.set_req_vars(req)
    req.data=self.get_artist_playlist




  def view(self,req):
    "give track/album overview, where available, (else standard grace view)"
    self.update_for_viewing(req)
    req.overviewing=True
    if self.kind == 'artist':
      return self.artist_overview(req)
    elif self.kind=='album':
      return self.album_overview(req)
    elif self.kind=='track':
      return self.track_overview(req)
    elif self.kind == 'albums':
      return self.albums(req)
    # anything else...
    req.overviewing=False
    if self.kind == 'artists':
      return self.artists(req)
    return basePage.view(self,req)

  # don't show move option for artists

  def get_pageoptions(self, req):
    " override the grace version to exclude move/copy option for artists"
    options = basePage.get_pageoptions(self, req)
    if self.kind == "artist":
  #    print(options)
       tab=options[-1] # assume the move is the last tab
       if tab[0]=="move/copy":
            options.pop(-1)
    return options


  # calculate time remaining

  def time_left(self):
    """gives time remaining for the current track (not necessarily self) in milliseconds
      - allows for override by "stoptime" preference (stored in self.transport.stoptime in milliseconds)
      - when there's no stoptime, transport.get_length() is preferred to the unreliable self.length
      """
    t=self.transport
    return (t.stoptime or t.get_length())-t.get_time()


#  @html
#  def album_select(self,req):
#    "album selection by thumbnail"
# #   req.wrapper="wrapper_subtle.evo"
#    prefob=self.get(2) # preferences for this are stored in page 2
#    req.data=self.list(kind='album',where='rating>=%s' % prefob.get_pref('min_rating'),orderby=prefob.get_order())
#    req.title="albums"

  @html
  def albums(self,req):
    "album listing by score or whatever"
    limit=page(req)
    req.pages=self.list(kind='album',where=f'rating in {sql_list(self.rating_filter())}',orderby=self.get_order(),limit=limit)
    req.title=req.page="albums"
#    print ">>>>>>>>>>>>>>>>>", req.page, req.pagesize, len(req.pages)

  @html
  def artists(self,req):
    "artist listing by score or whatever"
    limit=page(req)
    match=req.match and f"and name like '{req.match}%'" or ""
    where=f"(rating in {sql_list(self.rating_filter())}) {match}"
    req.pages=self.list(kind="artist",where=where,orderby=self.get_order(),limit=limit)
    req.title=req.page="artists"



  # play logging ############################################

  def log_track(self,uid):
    "logs an action for the given track uid"
    # abort if play-order or if use of forward or back buttons has disallowed logging
    if (uid==1) or (not self.transport.log_play):
      return
    self.transport.log_play=True # re-allow logging of plays
    # do the logging
    when=DATE().time(sec=True)
    print(when,': track',uid,'play logged')
    logfile=open('logs/mp.log','a')
    logfile.write("%s played at %s 1\n" % (uid,when))
    logfile.close()
    # add to plays and tracks
    self.get(safeint(uid)).add_play(when,1)

  def log_play(self):
    "log a play if more than half way (or 5 minutes) through"
    self.transport.newtrack=False # reset the flag, just in case
    elapsed=self.transport.get_time()
    percent=(elapsed*100)//self.transport.get_length()
    uid=self.transport.uid
    # log a play if 50% or more elapsed, or over 5 minutes
    if uid and ((percent>=50) or (elapsed>(5*60*1000))):
      self.log_track(uid)

  def log(self,req):
    "for manually adding a play..."
    if self.kind=='track':
      self.log_track(self.uid)
    else:
      req.error="can only log a play for a track"
    return self.get(self.uid).view(req) # refetch to get updated plays



  # transport ###################################################

  def mrl(self):
    "get the audio file url ('media resource locator')"
    return str(self.file_loc(self.code))

  def get_index(self):
    "return the index (within self.player.list) of the currently playing item"
    m=self.transport.get_media()
    pos=self.player.list.index_of_item(m)
    return pos

  def goto(self,req):
    "jumps to given req.index (which is required); used by playlist displays"
    if self.player.list and req.index:
      self.log_play() # log track as played if it is more than 1/2 through or 5 mins through
      if self.player.play_item_at_index(safeint(req.index))==0: # if goto is successful
        self.player.is_paused=False
        # update self.transport properties for the new track
        self.store_newtrack_data() # process the track change without relogging
    return self.redirect(req,"auto")

  def play(self,req):
    "starts playing a new list"
    if self.kind in self.playablekinds:   
      # kill any existing vlc list (just in case)
      if self.player.list:
        self.log_play()
        self._stop()
      # load new playlist into vlc  
      # 1) create a new vlc playlist
      self.player.list=vlc.MediaList()
      # 2) populate it with our track url(s)
      self.player.list.lock()
      playlist=self.get_playlist()
      if not playlist:
        req.warning='empty playlist: nothing to play'
        return self.view(req)
      for i in playlist:
        self.player.list.add_media(i.prepare_media())
      self.player.list.unlock()
      # 3) publish it to the player
      self.player.set_media_list(self.player.list)
      self.player.list.uid=self.uid # store where the playlist came from
      # 4) set volume
# 
#      volume=safeint(self.get_pref("volume"))
##      print("volume pref is:", volume)
#      volume=50
#      self.transport.audio_set_volume(volume)
#
      # 5) and play!
      self.player.play()
#      print("volume after play is:", self.transport.audio_get_volume())
      #save playlist in root
      root=self.get(1)
      uids=[str(i.uid) for i in playlist]
      root.text="\n".join(uids)
      root.flush()
      # update self.transport properties for the new track
      self.store_newtrack_data()
      # and return
      return self.redirect(req,"auto")
    req.error='unable to play this item'  
    return self.view(req)

  def playnext(self,req):
    "add self to playlist as next item - a simple LIFO stack"
    if not self.player.list: # i.e. if not currently playing
      return self.play(req) # this trashes the root playlist and creates a new one - and matching vlc medialist - containing self only
    # find the current index position in medialist (this may get it wrong if there are duplicates... but what to do??)
    root=self.get(1)
    rootuids=root.get_dumb_playlist()
    ix=self.get_index()+1 # this should point to after the currently playing track
    sx=ix # save the starting index - we need it below
    # add track or playlist to vlc medialist
    self.player.list.lock()
    playlist=self.get_playlist()
    for i in playlist:
      self.player.list.insert_media(i.prepare_media(),ix)
      ix+=1
    self.player.list.unlock()
    self.player.list.uid=1 # we have a mixed list, so let it be "from" the play order itself
    # update the root playlist, by inserting the new item(s)
    uids=rootuids[:sx]+[i.uid for i in playlist]+rootuids[sx:]
    root.text="\n".join([str(i) for i in uids])
    root.flush()
    # and return
    req.message="%s inserted into playorder" % self.name
    if req.return_to:
      return req.redirect(req.return_to)
    return self.redirect(req,"")

  def playappend(self,req):
    "append self to end of playlist"
    root=self.get(1)
    rootuids=root.get_dumb_playlist()
    playlist=self.get_playlist()
    playlistuids=[i.uid for i in playlist]
    if self.player.list:
      # VLC is currently playing and thus has a medialist
      # Find the current index position in the medialist
      ix=len(self.player.list)# point to after the end of the playlist
      # add track or playlist to vlc medialist
      self.player.list.lock()
      for i in playlist:
        self.player.list.insert_media(i.prepare_media(),ix)
        ix+=1
      self.player.list.unlock()
      self.player.list.uid=1 # we have a mixed list, so let it be "from" the play order itself
    # append the new items to the root playlist
    uids=rootuids+playlistuids
    # store the playlist uids in root.text
    root.text="\n".join([str(i) for i in uids])
    root.flush()
    # and return
    req.message="%s appended to playorder" % self.name
    if req.return_to:
      return req.redirect(req.return_to)
    return self.redirect(req,"")

  def pause(self,req):
    "pauses and restarts"
    if self.player.is_playing():
      uid=self.uid_now_playing()
      self.player.is_paused=uid
      self.set_transport_uids(uid) # ensure these are correct
      self.player.pause()
    elif self.player.is_paused:
      self.player.play()
      self.player.is_paused=False
#      return self.redirect(req,"auto")
#      return self.auto(req)
    elif self.kind in self.playablekinds: # we have an out-of-date pause, so play the current page
      return self.play(req)
    return self.redirect(req,"auto")

  def _stop(self):
    "stops and clears the list"
    if self.player.list:
      self.player.stop()
      self.player.list.release()
      self.player.list=None
      self.player.is_paused=False
      self.set_transport_uids(False) # indicate that we are not currently playing

  def stop(self,req):
    "stop and clear the list"
#    print("stopping !!!!!!!!!!!!!!!!!!")
    self.log_play() # log track as played if it is more than 1/2 through or 5 mins through
    # explicitly cancel any looping
    self.player.mode=0 # cancel the indication
    self.player.set_playback_mode(self.player.mode)
    # do the stop
    self._stop()
    req.auto=False
    return self.redirect(req,"")

  def forward(self,req):
    "jump forward 10 secs"
    xtime=self.transport.get_time()
    self.transport.set_time(xtime+10000)
    return ""  # result of this function is not used
#    return self.redirect(req,"auto")
##    return self.auto(req)

  def backward(self,req):
    "jump backward 10 secs"
    xtime=self.transport.get_time()
    self.transport.set_time(xtime-10000)
    return ""  # result of this function is not used
#    return self.redirect(req,"auto")
##    return self.auto(req)

  def skip(self,req):
    "play next track in list"
    # log the existing track if relevant
    self.log_play()
    # do the skip
    # VLC has a bug (when loop is not set) whereby if you skip the last track, it keeps playing it and then loops! - so we fix that
    xuid=self.uid_now_playing()
    xtime=self.transport.get_time()
    self.player.next() # tell the player to do the skip....
    if self.player.mode!=1: #if not looping
      if (xuid==self.uid_now_playing()) and (xtime<=self.transport.get_time()): #  we must be on the last track, so stop
        self._stop()
    # update self.transport properties for the new track
    self.store_newtrack_data()
    return self.redirect(req,"auto")
#    return self.auto(req)

  def skipback(self,req):
    "play previous track in list"
    # log the existing track if relevant
    self.log_play()
    # do the skip
    self.player.previous()  # tell the player to do the skip....
    # update self.transport properties for the new track
    self.store_newtrack_data()
    return self.redirect(req,"auto")

  def shuffle(self,req):
    "shuffle and play"
    self.shuffling=True
    return self.play(req)  

  def loop(self,req):
    "toggle loop mode (to loop/unloop the entire play order)"
    if self.player.mode==1:
      self.player.mode=0 #vlc.PlaybackMode.default
    else:
      self.player.mode=1 #vlc.PlaybackMode.loop
    self.player.set_playback_mode(self.player.mode)
#    return self.redirect(req,"auto")
    return self.redirect(req)

  def repeat(self,req):
    "toggle repeat mode (to loop/unloop a single track)"
    if self.player.mode==2:
      self.player.mode=0 #vlc.PlaybackMode.default
    else:
      self.player.mode=2 #vlc.PlaybackMode.repeat 
    self.player.set_playback_mode(self.player.mode)
    if self.transport.uid:
      return self.redirect(req,"auto")
  #    return self.auto(req)
    return self.view(req)


# track data #############################################

  def get_track_data(self,path=""):
    "returns a dict of track meta data"
    filename=path or self.file_loc(self.code)
    media=vlc.Media(str(filename))
    media.parse()
    d=[str(media.get_meta(i) or "") for i in range(17)]
    length=media.get_duration()//1000
    size=os.stat(filename).st_size
    try:
      density=size//length  # very rough, as length includes the file header!
    except: # for some reason length is occasionally 0, giving divide by zero error....
      density=0
    data={
     'name':d[0],
     'artist':d[1],
     'genre':d[2],
     'album':d[4],
     'seq':d[5],
     'text':d[6],
     'when':d[8],   
     'arturl':d[15],
     'length':elapsed(length),          
     'size': size,
     'lossless': density>70000, #>100000 was failing...
     }
    media.release()
    return data

  @html
  def view_filedata(self,req):
    "show track file data on a separate page"
    req.data=self.get_track_data()

  def display_length(self,format="m:s"):
    "self.length formatted for display purposes; of a track, album, or playlist"
    return elapsed((self.length+500)//1000,format=format)

  def calc_length(self):
    '''calulate length for a track, album, or a dumb playlist
    - store this in page table for each kind as self.length
    - for tracks the length is taken from the media header
       - this is adjusted for starttime and stoptime preferences
    - for albums and playlists, the length is a sum of the track lengths
    - SLOW for tracks
      - this is why we store self.length
      - GIVES WRONG LENGTHS FOR SOME TRACKS: presumably their media headers are inaccurate
        - if so, can be overridden (to the nearest second) by setting the stoptime preference
    '''
    if self.kind=='track':
      xlen=self.length
      start=self.get_timepref('starttime')*1000
      stop=self.get_timepref('stoptime')*1000
      if stop: # we have a stoptime so base length on this
        self.length=stop-start
      else: # get the length reported by the media header
        filename=self.file_loc(self.code)
        media=vlc.Media(str(filename))
        media.parse()
        self.length=media.get_duration()-start
      if self.length!=xlen:
        self.flush()
    elif self.kind=='album':
      xlen=self.length
      self.length=self.sum(item='length',parent=self.uid,kind="track")
      if self.length!=xlen:
        self.flush()
    elif self.kind=='playlist':
      xlen=self.length
      self.length=sum([i.length for i in self.get_dumb_playlist(objects=True)])
      if self.length!=xlen:
        self.flush()

  def check_album_length(self):
    "check/fetch all child track lengths, and the album total length, for self - self must be an album"
    if self.kind=='album':
      c=0
      tracks=self.list(kind='track',parent=self.uid)
      for i in tracks:
        if not i.length:
          # fetch it from the media header
          i.calc_length()
          c+=1
      self.calc_length()  

  def update_score(self):
    """recalc total score for an album or artist
       - album and artist score should only be wrong if a track has been moved away or added (i.e. parent changed)
       - this ensures that they correctly reflect the play log
    """
    xscore=self.score
    if self.kind=='album':
      self.score=self.sum(item='score',parent=self.uid,kind="track")
    elif self.kind=='artist':
      self.score=self.sum(item='score',parent=self.uid,kind="album")
    if self.score!=xscore:
      self.flush()

########## charts ###############################

  @html
  def charts(self,req):
    """ chart display
    expects - req.data - a list of pages with added .plays properties
            - req.title: the name of the chart
            - req._pl_chartkind: the type of chart
            - req._pl_index - index of currently playing song (if relevant)
            - req._pl_len - number of items
            - req._pl_start - index of start of display items
            - req._pl_prevperiod as integer year/month eg 2014 or 201410
            - req._pl_nextperiod ditto
    eg see chart()
    """
    pass

  def prevperiod(self,period):
      "return prior integer year/month eg 2014 or 201410"
      if (period>9999) and str(period).endswith('01'):
        return period-89
      elif period:
        return period-1
      return 0

  def nextperiod(self,period):
      "return subsequent integer year/month eg 2016 or 201601"
      if (period>9999) and str(period).endswith('12'):
        return period+89
      elif period:
        return period+1
      return 0

  def get_chart_period(self,req):
    """return required period-related values based on req.period

      if req.period is specified as a year - eg 2014
      then give that year

      elif req.period is specified as an integer-date - eg 201403
      then give that full calender month

      else default to all-time period (ie 20140101 to date) 
    """
    now=int(DATE())
    period=INT(req.period) or now//100 # default to current month if no valid req.period
    if period>9999: # assume it is a month
      if period<(now//100): # a valid complete previous month
        prior=True# this is a previous month
      else:
        period=now//100 # default to current month
        prior=False
      start=period*100+1
      end=self.nextperiod(period)*100+1
    else: # assume it is a year
      if (period<(now//10000)): # a prior year
        prior=True# this is a previous year
      else:
        period=now//10000 # default to current year
        prior=False
      start=period*10000+101
      end=self.nextperiod(period)*10000+101
    return period,start,end,prior

  def period_chart(self,req,period,start,end,prior):
    """ generic monthly / annual / all-time chart
        expects req._pl_chartkind and self.sql
    """
    # fetch the raw data
    period,start,end,prior=self.get_chart_period(req)
    todate='' if prior else 'to date'
    year=int(str(period)[:4])
    now=int(DATE())
    if req.alltime and (period in (now//10000,now//100)):
        req.title=f"{req.alltime} {req._pl_chartkind}"
    elif period>9999:
        date=DATE(period*100+1)
        req.title=f"{req.alltime} {req._pl_chartkind} for {date.datetime.strftime('%B')} {year} {todate}"
    else:
        req.title=f"{req.alltime} {req._pl_chartkind} for {year} {todate}"
    raw=self.list(asObjects=False,sql=self.sql)
    # process the raw data, so it is ready for the template
    req.data=[]
    for i in raw:
        try:
          ob=self.get(i["page"])
          ob.plays=i["sum(times)"] # monthly score is stored temporarily as self.plays
          req.data.append(ob)
          # is this the currently playing/paused track?
          if self.player.list and (ob.uid == self.transport.uid):
            req._pl_index=ob.uid  # the display will use this to hilite the track 
        except: # we have a deleted item - ignore it
          pass
#      for i in req.data:
#        print(i.uid, i.name, i.times)
    # set more constants for the template to use
    req.period=period
    req._pl_prevperiod=self.prevperiod(period)
    if prior:
      req._pl_nextperiod=self.nextperiod(period) 
    req._pl_len=len(req.data)
    req._pl_start=0
    # and return the template
    return self.charts(req)

  def iso(self,when):
    "convert mysql date integer to iso format for mysqlite"
    return DATE(when).sql(time=False, quoted=False)

  def chart(self,req):
    "monthly or annual track charts"
    req._pl_chartkind=f"chart"
    period,start,end,prior=self.get_chart_period(req)
    if req.alltime:
        where=f"`when`<'{self.iso(end)}'"
    else:
        where=f"`when`>='{self.iso(start)}' and `when`<'{self.iso(end)}'"
#    print("WHERE: ",where)
    self.sql=f"""
        select page,sum(times)
        from plays
        where {where}
        group by page having sum(times)>1
        order by sum(times) desc
        limit {self.maxtracks};
        """
    return self.period_chart(req,period,start,end,prior)

  def album_chart(self,req):
    "monthly or annual album charts"
    req._pl_chartkind="album chart"
    period,start,end,prior=self.get_chart_period(req)
    if req.alltime:
        where=f"plays.`when`<'{self.iso(end)}'"
    else:
        where=f"plays.`when`>='{self.iso(start)}' and plays.`when`<'{self.iso(end)}'"
    self.sql=f"""
        select album.uid as page, sum(times)
        from plays inner join pages on plays.page=pages.uid
        inner join pages as album on album.uid=pages.parent
        where {where}
        group by album.uid having sum(times)>2
        order by sum(times) desc
        limit {self.maxtracks};
        """
    return self.period_chart(req,period,start,end,prior)

  def artist_chart(self,req):
    "monthly or annual artist charts"
    req._pl_chartkind="artist chart"
    period,start,end,prior=self.get_chart_period(req)
    if req.alltime:
        where=f"plays.`when`<'{self.iso(end)}'"
    else:
        where=f"plays.`when`>='{self.iso(start)}' and plays.`when`<'{self.iso(end)}'"
    self.sql=f"""
        select artist.uid as page, sum(times)
        from plays inner join pages on plays.page=pages.uid
        inner join pages as album on album.uid=pages.parent
        inner join pages as artist on artist.uid=album.parent
        where {where}
        group by artist.uid having sum(times)>2
        order by sum(times) desc
        limit {self.maxtracks};
        """
    return self.period_chart(req,period,start,end,prior)

  def charts_summary(self,req):
    "prepares the data for the charts summary, and returns the template"
    req.title="summary of track plays"
    req._pl_chartkind="charts summary"
    xperiod=req.period
    req.period=int(DATE())//100 # current month
    req.data=[]
    while True:
      # get one year's data
      months=[0]*12
      annual=0
      while True:
        plays=self.monthly_plays(req)
#        print(req.month,' plays = ',plays)
        year=req.period//100
        month=req.period%100
        months[month-1]=plays
        annual+=plays
#        print("year:",year," month:",month," months:",months," annual:",annual)
#        print("type of month: ",type(month))
        req.period=self.prevperiod(req.period)
        if month==1:
          break
      # and add it to req.data 
      if not annual:
        # or (INT(req.month)<201400): # O/S: the 201400 condition is specific to the original IHM dataset, but this should not affect any newer datasets
        break
      req.data.append((year,months,annual))
    # restore the previous req.period
    req.period=xperiod
    # and return the template
    return self.charts(req)

  def monthly_plays(self,req):
    "total plays for req.month - for use by charts_summary() "
    req.period,start,end,prior=self.get_chart_period(req)
    plays=INT(self.Play.sum(item="times",where=f"`when`>='{self.iso(start)}' and `when`<'{self.iso(end)}'"))
    return plays

######## additions ##################################

  def add_logs(self,req):
    "fetch play log(s) from music/logs/additions folder, and process"
    logfiles=[]# list of logs
    names=[] #list without paths 
    folder="logs/additions"
    for name in os.listdir(folder):
      if name.endswith(".log"):
        logfiles.append("%s/%s" % (folder,name))
        names.append(name)
    msg=self.fetch_plays(logfiles)      
    if not msg.startswith('0'):
      for i in names: # move them to logs folder
        os.rename(folder+"/"+i,'logs/'+i)
    return msg

  def add_music(self,req):
    '''
    fetch new music from additions folder, and allocate to new or existing albums and artists
        
    - assume that self.Config.additions_folder (default = "mp_additions") will hold any new audio files to be added
    - fetch (move) whatever audio files are there (including in subfolders) 
    - for each one:
      - new track page for every audio file (temporarily put them all  in untitled album in Compilations)
      - audio file is renamed and moved to data folder
      - get track meta-data
      - if there is artwork, add the image
      - if there is a genre, add that
    - finds a sensible place for each track, by analysing them:
      - if by existing album artist, add them there
      - elif got 2 or more with same album, and artists differ, then  add that album (if necessary) in Compilations, and put them there
      - elif got 2 or more with same album and artist, then add that album and artist, and put them there
      - anything left will remain in the untitled album in Compilations  
    '''
    # fetch some key data objects..
    hob=self.get(3) # get the parent page for the artists
    cob=self.get(4) # get the Compilations pseudo-artist (our default place for the new tracks)
    dobs=self.list(parent=4,name='',kind='album')  # get the default album - the untitled album in Compilations
    if dobs: # we have the default album
      dob=dobs[0]
    else: # we create it
      dob=cob.add_album("")
    additions=[]# list of new tracks and their given albums 
    # get a list of filepaths, extract audio files, create tracks for them, moving the files to the ~/site/file folder
    for path,dirs,files in os.walk(self.Config.additions_folder):
      #print path,dirs, files
      for name in files:
        exts=name.rsplit(".",1)
        if (len(exts)==2):
          ext=exts[1].lower()
        else:
          ext=""
        if ext in ('mp3','m4a','flac','wav','xm','dts','mts','ogm','ogg','a52','aac','ac3','oma','spx','mod','mp2','wma','mka','m4p'):
          # create track
          r=Req()
          r.kind='track'
          r.stage='posted'
          tob=dob.create_page(r)
          # get the meta (before we move the file, as vlc may use the old filename for info)
          filepath=path+'/'+name
          data=tob.get_track_data(path=filepath)
          # move & rename audio file
          tob.code= "%s.%s" % (tob.uid,ext)
          os.renames(filepath,tob.file_loc(tob.code)) #BEWARE: this will remove the mp_additions folder itself if it is now empty...
  #        tob.flush() # store the new code
  #        print tob.uid," === ",data
          tob.name=data['name']
          tob.artist=data['artist']
          tob.seq=safeint(data['seq'])
          tob.text=data['text']
          if 0<safeint(data['when'])<10000: # got the year
            tob.when="1-1-%s" % data['when']
          # store it all
          tob.flush()
          # get length
          tob.calc_length() 
          ## store genre, if valid # NOTE June 2023: NEEDS ADAPTED FOR NEW TAG CODE if we want this at all
          #if data['genre'] and (data['genre'] in self.get_tagnames()): # BREAKS with ERROR message: no get_tagnames
          #  tob.add_tag(data['genre'])
          # add to additions list
          tob.album=data['album'] # store this in tob, for now 
          tob.arturl=data.get('arturl')
          additions.append(tob) 
    # now relocate tracks to album and/or artist, as required       
    # split by album:
    albums={}
    for tob in additions:
      if albums.get(tob.album):
        albums[tob.album].append(tob)
      else:
        albums[tob.album]=[tob]
    # process by album
    for album,tobs in list(albums.items()):
      ok=True
      artist=tobs[0].artist
      mob=None # album object (may be set below)  SHOULD IT NOT ALWAYS BE SET?????
      for tob in tobs[1:]:
        if tob.artist!=artist:
          # differing artists, so leave in compilations
          ok=False
      if ok : # we have one album, one artist
        # look for the artist
        aob=None
        aobs=self.list(name=artist,kind='artist') 
        if aobs: # got it
          aob=aobs[0]
        elif len(tobs)>1:  # add it
          aob=hob.add_artist(artist) # create new artist
        if aob: # we have an artist, and 2 or more tracks...
          mob=aob.add_to_album(album,tobs) # add tracks to the album (creating album if required)  
      elif album:# we have one album, more than one artist
#        if len(tobs)==1:
#          album="" # force use of untitled when we only have one track... 
        # add it to the Compilations artist (creating album if required)  
        mob=cob.add_to_album(album,tobs)
      #(otherwise, we leave the track(s) where they are) 
      # get the images (if any)
      for tob in tobs:
        url=tob.arturl
        if url and (url!='None') and url.startswith("file"):
          path= urllib.request.url2pathname(url)[7:]
          if album: # add one image to the album
            if mob and not mob.get_images():
              mob.add_art(path)
            break #we only want one
          tob.add_art(path) # no album, add every track image
    # return some useful info
    if additions:
      req.message="%s tracks added" % len(additions) 
      req.additions=[i.uid for i in additions] #this will be used to hilite the new items in the display
      # set global filter to 0 if necessary - so that the additions are displayed
      ob=self.get(1)
      if ob.rating>0:
        ob.set_rating(0)
#    else:
#      req.warning="no new additions found"
    pl=self.list(parent=2,name='additions',kind='smartlist')
    if pl:
      return pl[0].view(req)
    req.warning=" 'additions' playlist not found"
    return self.view(req)

  def add_to_album(self,album,trackobs):
    ''' adds a list of track objects to a named album, creating it if necessary 
    - returns the album object
    - self must be the album artist
    ''' 
    # look for the album  
    mobs=self.list(name=album,parent=self.uid,kind='album')
    if mobs: # got it
        mob=mobs[0]
    else: # add it  
        mob=self.add_album(album)
    # move the tracks to the album
    for tob in trackobs:
        tob.parent=mob.uid
        tob.set_lineage(mob)
        tob.flush()
    # set the album length
    mob.calc_length()
    return mob

  def add_artist(self,name):
    "adds a new album artist to self"
    req=Req()
    req.kind='artist'
    req.name=name
    req.stage='posted'
    req.parent=self.uid
    req.rating=0
    req.code=''
    req.seq=0
    req.text=''
    aob=self.create_page(req)
    aob.set_pref('order_by','name')
    aob.flush()
    return aob
  
  def add_album(self,name):
    "adds a new album to self"
    req=Req()
    req.kind='album'
    req.name=name
    req.artist=self.name
    req.stage='posted'
    req.parent=self.uid
    req.rating=0
    req.code=''
    req.seq=0
    req.text=''
    aob=self.create_page(req)
    aob.set_pref('order_by','seq')
    aob.flush()
    return aob

  def add_art(self,path,size="500"):
    """ adapted from image_saved() in base/Page.py
    
    THIS CODE HAS BEEN PARTLY-UPDATED AND NOT TESTED AND IS RARELY USED, SO IS CURRENTLY DISABLED
    
    stores new image, and returns image object, else returns None 
    """
    return None # DISABLED!
    
    error=False
    if path:
      print("processing %s to %s" % (path,self.uid))
      f=open(path,'rb') 
      filedata=f.read()
      extension=(imghdr.what('',filedata) or path.rsplit(".")[-1].lower()).replace('jpeg','jpg')
      if not filedata:
        error= "NO IMAGE FOUND AT '%s'" % path
        print(error)
      elif extension in ('bmp','png'):
        filedata=self.convert_image(filedata)
        extension='jpg'
      elif extension not in ('gif','png','jpg','jpeg'):
        error="only JPEG, GIF, PNG, and BMP are supported"
        print(error)
      if not error:
        # create a new image page
        image=self.new()
        image.parent=self.uid
        image.kind='image'
        image.seq=0xFFFFFF#place at end of siblings
        # set default size 
        image.stage='right full %sx%s' % (size,size) #rest of stage data will be added on the fly later by get_stage_data() 
        image.set_lineage()
        image.code="%s.%s" % (image.uid,extension)
        image.when=DATE()
        image.flush() #store the image page
        image.renumber_siblings_by_kind()#keep them in order
        # save the image file
        image.save_file(filedata)
        # return
        print('image "%s" added' % image.code)
        return image
    return None


####### search extensions ####################

  @classmethod
  def search_extra_objects(cls,term):
    "include new columns and tags in search"
    obs=cls.list(where='artist like "%%%s%%" or composer like "%%%s%%"' % (term,term),orderby='uid desc')  
    tobs=[cls.get(i.page) for i in cls.Tag.list(name=term, orderby='uid desc')]
    return obs+tobs

######## utilities #################################

  @classmethod
  def halt(self,req):
    "forces the system to quit"
    #reactor.callFromThread(reactor.stop)
    return 'system halted'

  @classmethod
  def clone_data(self,req):
    """clones all music data from req.source to req.dest
     O/S should first pull in and set up the mp code and music db (DO THIS MANUALLY for now)
    """
    # source folder
    source=req.source or "/media/howie/archive/files/music/"
    # destination folder
    dest=req.dest or "/home/howie/files/music/"
    # clone the music files
    c=0
    for i in self.list(isin={'kind':("track","image","file")},orderby="uid"):
      c+=1
#      print c," uid:",i.uid," kind:",i.kind," loc:",i.file_folder()," name:",i.name
      subfolder=i.file_folder()
      destfolder=dest+subfolder
      if not os.path.exists(destfolder):
        os.makedirs(destfolder)
      shutil.copy2(source+subfolder+"/"+i.code,destfolder)
      print("added %s" % (dest+subfolder+"/"+i.code,))
    return "clone completed: %s files added" % c


  #  for use when replacing tracks and albums with better versions
  def move_info(self,req):
    """copies self data to req.to, for tracks and albums, and disables self
    EXPECTS req.to
    self must be an album or track
    - for tracks, COPIES track data
    - for albums, COPIES album data and all track data
    MOVES all track and album images
  
    BEWARE - relies on album track numbers being correct and fully corresponding

    NOTE: will have to run fix_scores() after copying all required album/track info
          =========================================================================
    """
    def copy_item(st,dt):
        "copy info from st to dt"
        # move plays
#        print "moving plays for ",st.uid," to ",dt.uid
        execute("update plays set page=%s where page=%s " % (self.database,dt.uid,st.uid))
        # move tags 
        execute("update tags set page=%s where page=%s " % (self.database,dt.uid,st.uid))
        # copy info
        dt.name=st.name
        dt.when=st.when
        dt.composer=st.composer
        dt.artist=st.artist
        dt.text=st.text
        dt.rating=st.rating
        dt.prefs=st.prefs
        #dt.score=st.score
        dt.flush()
        st.name=st.name+" (old version)"
        st.rating= -4 #  set to X 
        st.flush()
        # move images
        st.get_images() # create st.images
        for i in  st.images:
          i.parent=dt.uid
          i.set_lineage(pob=dt)
          i.flush()
    try:
      dob=self.get(safeint(req.to))
    except:
      dob=None
    if (not dob):
      return "specify destination as ?to=[UID]"
    elif (self.kind!=dob.kind):
      return "source is a %s but destination is a %s" % (self.kind,dob.kind)  
    if self.parent!=dob.parent:
      return "source and destination parent mismatch"
    if self.kind=='album':
      copy_item(self,dob)
#      for st in self.list(parent=self.uid,kind='track',where="rating>=0",orderby="uid"):
      for st in self.list(parent=self.uid,kind='track',orderby="uid"):
        dt=dob.list(parent=dob.uid,kind="track",seq=st.seq) # get corresponding track from dob
        if dt:
          copy_item(st,dt[0])
    elif self.kind=='track':
      copy_item(self,dob)
    else:
      return "not an album or track..."
    req.message="info copied/moved to %s" % dob.uid
    return self.view(req)


######## ad hoc and tests #################################

  @classmethod
  def test(self,req):
    ""
    m=self.transport.get_media()
    pos=self.player.list.index_of_item(m)
    print("pos= %s" % pos)
    return "pos= %s" % pos

  def quo(self,req):
    "apply blockquotes and carriage returns to all of self.text"
    paras=self.text.splitlines()
    newparas=[]
    for p in paras:
      p=f"> {p}  "
      newparas.append(p)
    self.text="\n".join(newparas)
    self.flush()
    return self.view(req)

  def reset_length(self,req):
    ""
    self.calc_length()
    return self.view(req)

# not currently used
#
#  def fix_length(self,req):
#    """ a TEMPORARY bodge-fix for sorting the length of a currently playing track
#       Length is taken via self.transport.get_length() which seems to be reliable...
#       The track lengths are initially copied from the audio file headers, and these are often wrong... hence this fix is required.
#    """
#    uid=self.transport.uid
#    if uid:
#      track=self.get(uid)
#      length= self.transport.get_length()
#      if track.length!=length:
#        track.length=length
#        track.flush()
#        req.message=f"track {track.uid} length fixed to {length}"
#        print(req.message)
#      else:
#        req.message=f"track.length is already correct, at {length}"
#        print(req.message)
#    return self.auto(req)


  def tidy_tags(self,req):
    "removes any obsolete tags - THERE SHOULD NOT BE ANY!"
    count=0
    for t in self.Tag.list():
      if not self.exists(t.page):
        t.delete()
        count+=1
    return "%s obsolete tags deleted" % count    

#  def fix_tags(self,req):
#    "ad hoc..."
#    n=0
#    for t in self.list(where="uid>9772",kind='track'):
#      t.add_tag('Chillout')
#      n=n+1 
#    return "%s tags added" % n  
   

#  def fix_ratings(self,req):
#    "ONE_OFF UTILITY"
#    for i in self.list(kind='track'):
#      if (i.rating==-1) and (not i.parent in (5232,1536,1795,1877)):
#        i.rating=0
#        i.flush()
#    req.message='ratings fixed'
#    return self.view(req)

#  def fix_startstop(self,req):
##    "ONE_OFF UTILITY works but of no use because VLC start and stop parameters are in seconds not milliseconds :("
#    o="fixing starttimes and stoptimes <br/>"
#    for i in self.list(kind='track'):
#      start=i.get_pref("starttime")
#      stop=i.get_pref("stoptime")
#      if start or stop:
#        o+=f"{i.uid}: start was {start} stop was {stop} <br/>"
#        start= int(start or 0)*1000
#        stop= int(stop or 0)*1000
#        if start:
#          i.set_pref("starttime",start)
#          i.flush()
#        if stop:
#          i.set_pref("stoptime",stop)
#          i.flush()
#        o+=(f"{i.uid}: start now {i.get_pref('starttime')} stop now {i.get_pref('stoptime')} <br/>")
#    o+="starttimes and stoptimes all fixed <br />"
#    return o
    

# disabled as will destroy any track length fixes (fix_lengths uses calc_length which gets its length data from the untrustworthy audio file headers)
#  @classmethod
#  def fix_lengths(self,req):
#    "utility to fix all track and album lengths"
#    c=0
#    for t in self.list(kind='track'):
#      if not t.length: # for speed, assume if we have a length that it is correct (as audio files are never altered here)
#        t.calc_length()
#        c+=1
#    for aob in self.list(kind='album'):
#      aob.calc_length()
#    return "%s track lengths added - all album lengths updated" % c

  @classmethod
  def fix_summary_scores(cls,req):
    ""
    for i in cls.list(isin={"kind":['artist','album']}):
      i.update_score()
    return "artist and album score totals updated"

  def recalc_score(self):
    """ re-calculates plays for one track
      - fetches plays from the plays table
      - stores the result in the pages table
      (self must be a track or nothing happens)
      (returns nothing - but self.score is updated)
    """
    if self.kind=="track":
      self.score=0
      for i in self.Play.list(page=self.uid):
        self.score+=i.times
      self.flush()

  def fix_score(self,req):
    """ re-calculate plays (i.e. score)
      - for one track from the plays table
      - or for one album or artist from the child scores
      (self must be a track, artist, or album)
    """
    if self.kind in ("album","artist"):
      self.update_score()
      req.message="score reset from child scores"
    elif self.kind=="track":
      self.recalc_score()
      req.message="score reset from plays table"
    else:
      req.error= "not a track, album, or artist"
    return self.view(req)

  fix_plays = fix_score # alias

  def move_plays(self,req):
    "move plays from self to req.to - for tracks only - updating the scores"
    if self.kind=="track":
      if req.to:
        tob=self.get(req.to)
        if tob and tob.kind=="track":
          execute("update plays set page=%s where page=%s " % (tob.uid,self.uid))
          self.recalc_score()
          tob.recalc_score()
          req.message="plays moved to track %s" % tob.uid
        else:
          req.warning="nothing done as valid track-uid is required"
      else:
        req.warning="nothing done - '?to=' track-uid is required"
    else:
      req.warning="nothing done as this is not a track"
    return self.view(req)

  def origin_uid(self):
    "returns origin uid for self"
    return safeint(self.code.rsplit(".",1)[0])

  def pull_plays(self,req):
    """ (for a track) if cloned, pulls all plays from origin track
      - (for an album) pulls plays for any/all clone tracks
      - converts the plays in the plays table
      - updates the scores of both clone(s) and origin(s)
      - deals only with clone tracks
      WARNING: NOT TRULY REVERSABLE
    """
    # get list of tracks
    tracks=[]
    if self.kind == "track" and (self.origin_uid()!=self.uid):
      tracks=[self]
    elif self.kind == "album":
      for i in self.list(parent=self.uid, kind="track"):
        if i.origin_uid()!=i.uid:
          tracks.append(i)
    # pull the plays from the origin
    for i in tracks:
      # adjust the plays
      origin=i.origin_uid()
      execute(f"update plays set page={i.uid} where page={origin}")
      # calc and store the new score
      i.recalc_score()
      self.get(origin).recalc_score()
    # report
    if tracks:
      req.message= "plays pulled from origin"
    else:
      req.warning= "no clone track found"
#    req.message = str([i.uid for i in tracks])
#    req.message= str(self.origin_uid())
    return self.view(req)

  def push_plays(self,req):
    """ (for a track) if cloned, pushes all plays to origin track
      - (for an album) pushes plays for any/all clone tracks
      - converts the plays in the plays table
      - updates the scores of both clone(s) and origin(s)
      - deals only with clone tracks
      WARNING: NOT TRULY REVERSABLE
      Note:mostly identical to def pull_plays(), so could easily be combined…
    """
    # get list of tracks
    tracks=[]
    if self.kind == "track" and (self.origin_uid()!=self.uid):
      tracks=[self]
    elif self.kind == "album":
      for i in self.list(parent=self.uid, kind="track"):
        if i.origin_uid()!=i.uid:
          tracks.append(i)
    # push the plays to the origin
    for i in tracks:
      # grab (adjust) the plays
      origin=i.origin_uid()
      execute(f"update plays set page={origin} where page={i.uid}")
      # calc and store the new score
      i.recalc_score()
      self.get(origin).recalc_score()
    # report
    if tracks:
      req.message= "plays pushed to origin"
    else:
      req.warning= "no clone track found"
    return self.view(req)


  @classmethod
  def fix_all_scores(cls,req):
    """ crudely but simply re-calculates plays (i.e. score)
       - gets play totals for every track record from plays table
       - then re-calculates all album and artist summary scores
    """
#    if from_uid==1:
    print("resetting scores to zero...")
    cls.list(asObjects=False,sql="update `%s`.pages set score=0" % cls.Config.database)
    print("adding play scores ...")
#    for i in cls.Play.list(where= ("uid>=%s" % req.from_uid) if (req.from_uid>1) else ""):
    for i in cls.Play.list():
      try:
        tob=cls.get(i.page)
        tob.score=tob.score+i.times
        tob.flush()
#      except Exception as e:
#        print "ERROR with ",i.page,' : ',e
      except:
        print("deleting %s play(s) for missing track %s" % (i.times,i.page))
        i.delete() # delete invalid plays (presumably the track is already deleted)
    print("calculating summary scores")
    cls.fix_summary_scores(req)
    print("done")
    return "all scores reset from plays table"

  def extract_version(self):
    "one-off script to extract version information from and album or track and put it in version field"
    if self.kind not in ['album','track']:
      return False
    lines=self.text.split('\n')
    text=[]
    self.version=""
    for l in lines:
      if (not self.version) and (l.lstrip()[0:2]=='* '): #likely version info
        self.version=l.strip()[2:]
        print('got version for ', self.kind, self.uid, ' : ', self.version)
      else:
        text.append(l)
    if self.version:
      self.text='\n'.join(text)
      self.flush()
    return True

#  def get_pics(self,req):
#    '''
#     sensible defaults eg 500x500 for albums, 300x300 for tracks
#    '''
#    req.error="DISABLED"
#    return self.view(req)
#
#    root=self.get(3)
#    count=0
#    for artist in root.get_children():
###     if artist.uid>7926:
##    artist=self.get(4)
#      for album in artist.get_children():
#       if not album.name: # second pass to get untitled track pics
#        for t in album.get_children():
#          print "====== checking %s",t.uid, t.name
#          data=t.get_track_data()  
#          path= data.get('arturl','') and data['arturl'].startswith("file") and urllib.url2pathname(data['arturl'])[7:] or ""
#          if path:
#            if t.pic_saved(path):
##            if album.pic_saved(req,path,size='500'):
#              count+=1
#              break 
#    req.message="%s pics added" % count          
#    return self.view(req)  

#  def set_album_artists(self,req):
#    ""
#    for i in self.list(kind='album'):
#      i.artist=i.get_pob().name
#      i.flush()
#    return self.view(req)

#  def create_genre_playlists(self,req):
#    "ONE OFF UTILITY to generate genre playlists - call from genres page"
#    for genre in self.get_tagnames():
#          # create playlist
#          r=Req()
#          r.name=genre
#          r.kind='smartlist'
#          r.stage='posted'
#          r.rating=1 
#          ob=self.create_page(r) 
#          ob.add_tag(genre)
#          ob.flush
#    return self.view(req)      

#  def rate_children(self,req):
#    "for playlists and genres kinds only"
#    if self.kind in ('playlists','genres'):
#      for i in self.get_children():
#        i.rating=1
#        i.flush()
#    return self.view(req)      


 # def derive_album_tags(self):
 #   "ONE-OFF UTILITY for use with derive_all_album_tags() -  get an album's tags from its track tags"
 #   tags=self.get_tags() # get existing album tags
 #   # get track tags
 #   tracks=[i.uid for i in self.get_children_by_kind(kind='track')]
 #   print 'uid=',self.uid,' tracks= ',tracks
 #   if tracks:
 #     tracktags=[i['name'] for i in self.Tag.list(asObjects=False,isin={'page':tracks},what='distinct name')]
 #     print 'uid=',self.uid,' tracktags= ',tracktags
 #     # add any new ones
 #     for t in tracktags:
 #       if not t in tags:
 #         self.add_tag(t)
 #         print 'tag %s added' % t
 #     return True
 #   return False    
           
#  def derive_all_album_tags(self,req):
#    "ONE-OFF UTILITY derive tags for every album" 
#    count=0
#    for album in self.list(kind='album'):
#      if album.derive_album_tags():
#        count+=1
#    req.message="tags derived from tracks for %s albums" % count  
#    return self.view(req)    



# The following has been disabled as player.is_shuffling is no longer in use.
# It works and can be reused for other variables if/when required
#

## SAVE AND RESTORE STATE AT-EXIT #################################
##
## can't use Var() because it is not accessible yet (during module load), so just use a file "state"
#
#def save_state():
#    ""
#    f=open("state",'w')
#    f.write("is_shuffling=%s" % (Page().player.is_shuffling and 1 or 0))
#    f.close()
#    
#def restore_state():
#    ""
#    try:
#      f=open("state",'r')
#      name,val=f.readline().split('=')
#      Page().player.is_shuffling=bool(safeint(val))
#      f.close()
#    except:
#      raise
#      pass
# 
#import atexit
#restore_state()
#atexit.register(save_state)

