"""
config file for a grace music (mp) app

see ../grace/config.py for available options to override here

note that config is loaded in this order:
- grace/config.py
- <appname>/config.py
"""

domains=["mp","localhost","127.0.0.1"]
#domain = 'localhost'  #if not provided,  this is set to domains[0] by serve/app.py
#database = ''  #this will default to <app name>.db (no path - ie in the current working directory)
port = '9006' # default is 8080

urlhost="%s:%s" % (domains[0],port)

# additions_folder="/home/howie/mp_additions"
additions_folder="/home/howie/files/music/additions"


#default_class="Page"
#urlpath=""  # no /evoke in url

meta_description="music player, VLC, music database" #site description text for search engines
meta_keywords="music, VLC" #comma separated list of keywords for search engines

from grace.data.schema import *

class Play(Schema):
  table='plays'
  when=DATE,now,KEY
  page=INT,KEY
  times=INT

from Page.config import *

