# more complex version of def toggle_disable() which allows double-disabling (and re-enable) of tracks and albums which were already disabled
#
# AN ATTEMPT TO SOLVE:
# 1. When disabling an albums, if any tracks were already disabled, that distinction is now lost - as a re-enable of the album will re-enable ALL tracks
# 2. Whin disabling an artist, if any album was already disabled, that distinction is now lost (as for the tracks)
#
# PROBLEMS:
# 1. disabling/enabling  is a 2 way toggle, whereas double disabling/enabling HERE is a 3 way toggle and so cannot be elegantly controlled manually for individual items
#     and so individual tracks or albums cannot be double-disabled by hand (only via the disabling of their parent)
# 2. we are allowing 3 levels of nesting (artist, album, track) and so a trible disable/enable stage (for tracks) is also required to cover all eventualities
#
# Because of the over complexity of this, I have abandoned it for now
#
# This version covers only double enabling, and on testing there were various problems, which I can't see an elegant solution for
#
# CONCLUSION:
#
# KEEP IT SIMPLE (no double disabling). As a result:
# 1. when disabling an album, any existing disabling of individual tracks is effectively lost, as a re-enable of the album will re-enable ALL tracks.
# 2. when disabling an artist, all existing disabling of albums and/or tracks is effectively lost, as a re-enable of teh artist will re-enable ALL their albums and tracks
#
# CONSEQUENCES: Don't use disabling of individual tracks or albums to indicate that hese ashould be supressed. Mark them deleted instead 
# Note: deleted items are not affected by this toggle_disable
#



  # evoke.Page.ratings.py override:

  def toggle_disable(self,req):
    """ override Page.ratings.py version to:
        - disable/enable all albums when disabling/enabling an artist
        - disable/enable all tracks when disabling/enabling an album
        - thus disabling an artist will disable all of its albums and all of its tracks
        - deleted items (rating is -5) are unaffected
        - BEWARE: when disabling, child items already disabled will lose that distinction, and will be re-enabled when the parent is re-enabled
    """
    enabled=(0,1,2,3)
    disabled=(-4,-3,-2,-1)
    doubledisabled=(-9,-8,-7,-6)

    def disable_tracks(album):
        "disable all tracks for self"
        for track in self.list(parent=album.uid,kind="track"):
          if track.rating in enabled:
            track.rating-=4  # disable
          elif track.rating in disabled:
            track.rating-=5 # double disable
          track.flush()

    def enable_tracks(album):
        "enable all tracks for self"
        for track in self.list(parent=album.uid,kind="track"):
          if track.rating in disabled:
            track.rating+=4 # enable
          elif track.rating in doubledisabled:
            track.rating+=5 # single disable
          track.flush()

    def disable_albums(artist):
        "disable all albums and their tracks for self"
        for album in self.list(parent=artist.uid,kind="album"):
          if album.rating in enabled:
            album.rating-=4 # disable
          elif album.rating in disabled:
            album.rating-=5 # double disable
          album.flush()
          disable_tracks(album)

    def enable_albums(artist):
        "enable all albums and their tracks for self"
        for album in self.list(parent=artist.uid,kind="album"):
          if album.rating in disabled:
            album.rating+=4 # enable
          elif album.rating in doubledisabled:
            album.rating+=5 # single disable
          album.flush()
          enable_tracks(album)

    if self.kind=="artist":
        # disable or re-enable all albums and their tracks for this artist
        if self.rating in enabled:
          disable_albums(self)
        elif self.rating in disabled:
          enable_albums(self)
    elif self.kind=="album":
        # disable or re-enable all tracks for this album
        if self.rating in enabled:
          disable_tracks(self)
        elif self.rating in disabled:
          enable_tracks(self)
    return basePage.toggle_disable(self,req)

